package fallingstars.fallingdroid.net;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;

import fallingstars.fallingdroid.content.Authorization;
import fallingstars.fallingdroid.util.ResourceReader;

public class AuthorizationReader implements ResourceReader<Authorization> {
    private static final String TAG = AuthorizationReader.class.getSimpleName();

    public static final String AUTHORIZATION = "Authorization";

    @Override
    public Authorization read(JsonReader reader) throws IOException {
        Authorization authorization = new Authorization();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(AUTHORIZATION)) {
                authorization.setToken(reader.nextString());
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("Property '%s' ignored", name));
            }
        }
        reader.endObject();
        return authorization;
    }
}
