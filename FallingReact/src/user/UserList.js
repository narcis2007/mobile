import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator} from 'react-native';
import {UserEdit} from './UserEdit';
import {UserView} from './UserView';
import {loadUsers, cancelLoadUsers} from './service';
import {registerRightAction, getLogger, issueText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('UserList');
const USER_LIST_ROUTE = 'user/list';

export class UserList extends Component {
    static get routeName() {
        return USER_LIST_ROUTE;
    }

    static get route() {
        return {name: USER_LIST_ROUTE, title: 'User List', rightText: 'New'};
    }

    constructor(props) {
        super(props);
        log('constructor');
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});
        const userState = this.props.store.getState().user;
        this.state = {isLoading: userState.isLoading, dataSource: this.ds.cloneWithRows(userState.items)};
        registerRightAction(this.props.navigator, this.onNewUser.bind(this));
    }

    render() {
        log('render');
        let message = issueText(this.state.issue);
        return (
            <View style={styles.content}>
                { this.state.isLoading &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text>{message}</Text>}
                <ListView
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={user => (<UserView user={user} onPress={(user) => this.onUserPress(user)}/>)}/>
            </View>
        );
    }

    onNewUser() {
        log('onNewUser');
        this.props.navigator.push({...UserEdit.route});
    }

    onUserPress(user) {
        log('onUserPress');
        this.props.navigator.push({...UserEdit.route, data: user});
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const userState = store.getState().user;
            this.setState({dataSource: this.ds.cloneWithRows(userState.items), isLoading: userState.isLoading});
        });
        store.dispatch(loadUsers());
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        this.props.store.dispatch(cancelLoadUsers());
    }
}
