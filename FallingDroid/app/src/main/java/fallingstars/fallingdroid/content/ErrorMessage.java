package fallingstars.fallingdroid.content;

/**
 * Created by Narcis2007 on 17.11.2016.
 */

public class ErrorMessage {
    String message;

    public ErrorMessage(){
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
