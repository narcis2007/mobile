package fallingstars.fallingdroid.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import fallingstars.fallingdroid.FallingApp;
import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.OnActionCompletedListener;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.content.UserClickHandler;
import fallingstars.fallingdroid.util.OkAsyncTaskLoader;
import fallingstars.fallingdroid.util.OkCancellableCall;
import fallingstars.fallingdroid.util.OnErrorListener;
import fallingstars.fallingdroid.util.OnSuccessListener;
import fallingstars.fallingdroid.widget.UserListAdapter;

public class UserListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<User>> {

    public static final String TAG = UserListFragment.class.getSimpleName();
    private static final int USER_LOADER_ID = 1;
    private UserClickHandler onClickHandler;
    private FallingApp mApp;
    private UserListAdapter mUserListAdapter;
    private AsyncTask<String, Void, List<User>> mGetUsersAsyncTask;
    private ListView mUserListView;
    private View mContentLoadingView;
    private boolean mContentLoaded = false;
    private OkCancellableCall mGetUsersAsyncCall;
    private OnActionCompletedListener<Object> onAdd;

    public UserListFragment() {
    }

    public void setOnClickHandler(UserClickHandler onClickHandler) {
        this.onClickHandler=onClickHandler;
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mApp = (FallingApp) context.getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View layout = inflater.inflate(R.layout.fragment_users, container, false);
        mUserListView = (ListView) layout.findViewById(R.id.user_list);
        mContentLoadingView = layout.findViewById(R.id.content_loading);
        showLoadingIndicator();


        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        //getLoaderManager().initLoader(NOTE_LOADER_ID, null, this); //:) :(
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        startGetUsersAsyncCall(); //:)
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.addUserFB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAdd.completed(null);
            }
        });
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        ensureGetUsersAsyncCallCancelled();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        showLoadingIndicator();
        return mApp.getUserManager().getUsersLoader();
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> users) {
        if (loader instanceof OkAsyncTaskLoader) {
            Exception loadingException = ((OkAsyncTaskLoader) loader).loadingException;
            if (loadingException != null) {
                Log.e(TAG, "Get users failed");
                showError(loadingException);
                return;
            }
            showContent(users);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {
        // not used
    }

    private void startGetUsersAsyncCall() {
        if (mContentLoaded) {
            Log.d(TAG, "startGetNotesAsyncCall - content already loaded, return");
            return;
        }
        mGetUsersAsyncCall = mApp.getUserManager().getUsersAsync(
                new OnSuccessListener<List<User>>() {
                    @Override
                    public void onSuccess(final List<User> users) {
                        Log.d(TAG, "startGetUsersAsyncCall - success");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showContent(users);
                            }
                        });
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(Exception e) {
                        Log.d(TAG, "startGetUsersAsyncCall - error");
                        showError(e);
                    }
                }
        );
    }

    private void ensureGetUsersAsyncCallCancelled() {
        if (mGetUsersAsyncCall != null) {
            Log.d(TAG, "ensureGetUsersAsyncCallCancelled - cancelling the task");
            mGetUsersAsyncCall.cancel();
        }
    }


    private void showError(Exception e) {
        Log.e(TAG, "showError", e);
        new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setMessage(e.getMessage())
                .setCancelable(true)
                .create()
                .show();
    }

    private void showLoadingIndicator() {
        Log.d(TAG, "showLoadingIndicator");
        mUserListView.setVisibility(View.GONE);
        mContentLoadingView.setVisibility(View.VISIBLE);
    }

    private void showContent(final List<User> users) {
        Log.d(TAG, "showContent");
        mUserListAdapter = new UserListAdapter(this.getContext(), users,onClickHandler);
        mUserListView.setAdapter(mUserListAdapter);
        mContentLoadingView.setVisibility(View.GONE);
        mUserListView.setVisibility(View.VISIBLE);
    }

    public void setOnAdd(OnActionCompletedListener<Object> onAdd) {
        this.onAdd = onAdd;
    }
}
