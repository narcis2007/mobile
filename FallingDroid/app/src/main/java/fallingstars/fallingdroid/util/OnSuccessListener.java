package fallingstars.fallingdroid.util;

public interface OnSuccessListener<E> {
    void onSuccess(E e);
}
