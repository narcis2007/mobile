package fallingstars.fallingdroid;

import android.app.Application;
import android.util.Log;

import fallingstars.fallingdroid.net.UserRestClient;
import fallingstars.fallingdroid.service.UserManager;

/**
 * Created by Narcis2007 on 20.10.2016.
 */

public class FallingApp extends Application {

    public static final String TAG = FallingApp.class.getSimpleName();
    private UserManager userManager;
    private UserRestClient userRestClient;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        userManager = new UserManager(this);
        userRestClient = new UserRestClient(this);
        userManager.setUserRestClient(userRestClient);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }

    public UserManager getUserManager() {
        return userManager;
    }


}
