package fallingstars.fallingdroid.service;

import android.content.Context;
import android.util.Log;

import java.util.List;

import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.net.UserRestClient;
import fallingstars.fallingdroid.util.OkAsyncTaskLoader;

public class UsersLoader extends OkAsyncTaskLoader<List<User>> {
    private static final String TAG = UsersLoader.class.getSimpleName();
    private final UserRestClient mUserRestClient;
    private List<User> notes;

    public UsersLoader(Context context, UserRestClient noteRestClient) {
        super(context);
        mUserRestClient = noteRestClient;
    }

    @Override
    public List<User> tryLoadInBackground() throws Exception {
        Log.d(TAG, "tryLoadInBackground");
        notes = mUserRestClient.getAll();
        return notes;
    }

    @Override
    protected void onStartLoading() {
        if (notes != null) {
            Log.d(TAG, "onStartLoading - deliver result");
            deliverResult(notes);
        }

        if (takeContentChanged() || notes == null) {
            Log.d(TAG, "onStartLoading - force load");
            forceLoad();
        }
    }
}
