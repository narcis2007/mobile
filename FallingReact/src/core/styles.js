import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  content: {
    marginTop: 70,
    flex: 1
  },
  activityIndicator: {
    height: 50
  },
  deleteButton: {
    position: 'absolute',
    bottom:10,
    right:10,
    backgroundColor: 'orange',
    flex: 0.5
  }
});

export default styles;