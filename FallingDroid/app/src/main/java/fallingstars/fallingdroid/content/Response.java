package fallingstars.fallingdroid.content;

/**
 * Created by Narcis2007 on 21.11.2016.
 */

public class Response {
    boolean success;

    public Response(){

    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
