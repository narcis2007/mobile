package fallingstars.fallingdroid.net;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;

import fallingstars.fallingdroid.content.ErrorMessage;
import fallingstars.fallingdroid.util.ResourceReader;

public class ErrorMessageReader implements ResourceReader<ErrorMessage> {
    private static final String TAG = ErrorMessageReader.class.getSimpleName();

    public static final String MESSAGE = "message";

    @Override
    public ErrorMessage read(JsonReader reader) throws IOException {
        ErrorMessage errorMessage = new ErrorMessage();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(MESSAGE)) {
                errorMessage.setMessage(reader.nextString());
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("Property '%s' ignored", name));
            }
        }
        reader.endObject();
        return errorMessage;
    }
}
