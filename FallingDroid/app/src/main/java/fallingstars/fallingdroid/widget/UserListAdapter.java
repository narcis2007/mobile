package fallingstars.fallingdroid.widget;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.content.UserClickHandler;

public class UserListAdapter extends BaseAdapter{//, ListActivity
    public static final String TAG = UserListAdapter.class.getSimpleName();
    private final Context mContext;
    private final UserClickHandler onClickHandler;
    private List<User> mUsers;

    public UserListAdapter(Context context, List<User> users, UserClickHandler onClickHandler) {
        mContext = context;
        this.onClickHandler=onClickHandler;
        mUsers = users;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View userLayout = LayoutInflater.from(mContext).inflate(R.layout.user, null);
        userLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickHandler.handle((mUsers.get(position)));

            }
        });
        ((TextView) userLayout.findViewById(R.id.username)).setText(mUsers.get(position).getUsername());
        Log.d(TAG, "getView " + position);
        return userLayout;
    }

    public void refresh() {
        notifyDataSetChanged();
    }
}
