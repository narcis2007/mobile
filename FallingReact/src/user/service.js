import {getLogger} from '../core/utils';
import {apiUrl, authHeaders} from '../core/api';
const log = getLogger('user/service');
const action = (type, payload) => ({type, payload});

const SAVE_USERS_STARTED = 'user/saveStarted';
const SAVE_USERS_SUCCEEDED = 'user/saveSucceeded';
const SAVE_USERS_FAILED = 'user/saveFailed';
const CANCEL_SAVE_USERS = 'user/cancelSave';

const LOAD_USERS_STARTED = 'user/loadStarted';
const LOAD_USERS_SUCCEEDED = 'user/loadSucceeded';
const LOAD_USERS_FAILED = 'user/loadFailed';
const CANCEL_LOAD_USERS = 'user/cancelLoad';

const DELETE_USER_FAILED = 'user/deleteFailed';
const DELETE_USER_STARTED = 'user/deleteStarted';
const DELETE_USER_SUCCEEDED = 'user/deleteSucceeded';


export const loadUsers = () => (dispatch, getState) => {
  log(`loadUsers started`);
  dispatch(action(LOAD_USERS_STARTED));
  let ok = false;
  return fetch(`${apiUrl}/user/all`, {method: 'GET', headers: authHeaders(getState().auth.token)})
    .then(res => {
      ok = res.ok;
      return res.json();
    })
    .then(json => {
      log(`loadUsers ok: ${ok}, json: ${JSON.stringify(json)}`);
      if (!getState().user.isLoadingCancelled) {

        dispatch(action(ok ? LOAD_USERS_SUCCEEDED : LOAD_USERS_FAILED, json));
      }
    })
    .catch(err => {
      log(`loadUsers err = ${err.message}`);
      if (!getState().user.isLoadingCancelled) {
        dispatch(action(LOAD_USERS_FAILED, {issue: [{error: err.message}]}));
      }
    });
};
export const cancelLoadUsers = () => action(CANCEL_LOAD_USERS);

export const saveUser = (user) => (dispatch, getState) => {
  const body = JSON.stringify(user);
  log(`saveUser started`);
  dispatch(action(SAVE_USERS_STARTED));
  let ok = false;
  const url = user.id ? `${apiUrl}/user/${user.username}` : `${apiUrl}/user`;
  const method = user.id ? `PUT` : `POST`;
  return fetch(url, {method, headers: authHeaders(getState().auth.token), body})
    .then(res => {
      ok = res.ok;
      return res.json();
    })
    .then(json => {
      log(`saveUser ok: ${ok}, json: ${JSON.stringify(json)}`);
      if (!getState().user.isSavingCancelled) {
        dispatch(action(ok ? SAVE_USERS_SUCCEEDED : SAVE_USERS_FAILED, json));
      }
    })
    .catch(err => {
      log(`saveUser err = ${err.message}`);
      if (!getState().isSavingCancelled) {
        dispatch(action(SAVE_USERS_FAILED, {issue: [{error: err.message}]}));
      }
    });
};
export const cancelSaveUsers = () => action(CANCEL_SAVE_USERS);

export const deleteUser = (user) => (dispatch, getState) => {
  log(`deleteUser started`);
  dispatch(action(DELETE_USER_STARTED));
  let ok = false;
  const url = `${apiUrl}/user/${user.username}`;
  const method = `DELETE`;
  return fetch(url, {method, headers: authHeaders(getState().auth.token)})
      .then(res => {
        ok = res.ok;
        return res.json();
      })
      .then(json => {
        log(`deleteUser ok: ${ok}, json: ${JSON.stringify(json)}`);
        if (!getState().user.isDeletingCancelled) {
          dispatch(action(ok ? DELETE_USER_SUCCEEDED : DELETE_USER_FAILED, user));
        }
      })
      .catch(err => {
        log(`deleteUser err = ${err.message}`);
        if (!getState().isDeletingCancelled) {
          dispatch(action(DELETE_USER_FAILED, {issue: [{error: err.message}]}));
        }
      });
};

export const userReducer = (state = {items: [], isLoading: false, isSaving: false}, action) => { //newState (new object)
  switch(action.type) {
    case LOAD_USERS_STARTED:
      return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
    case LOAD_USERS_SUCCEEDED:{
      log("LOAD_USERS_SUCCEEDED, action: ");
      console.log(action.payload);
      return {...state, items: action.payload, isLoading: false};
    }
    case LOAD_USERS_FAILED:
      return {...state, issue: action.payload.message, isLoading: false};
    case CANCEL_LOAD_USERS:
      return {...state, isLoading: false, isLoadingCancelled: true};
    case SAVE_USERS_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case SAVE_USERS_SUCCEEDED:
      let items = [...state.items];
      let index = items.findIndex((i) => i.id == action.payload.id);
      log("index:"+index);
      if (index != -1) {
        items.splice(index, 1, action.payload);
      } else {
        items.push(action.payload);
      }
      return {...state, items:items, isSaving: false};
    case SAVE_USERS_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_SAVE_USERS:
      return {...state, isSaving: false, isSavingCancelled: true};

    case DELETE_USER_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case DELETE_USER_SUCCEEDED:
      var items = [...state.items];
      var index = items.findIndex((i) => i.username == action.payload.username);
      log("index:"+index);
      if (index > -1) {
        items.splice(index, 1);
      }
      return {...state, items:items, isSaving: false};
    case DELETE_USER_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_SAVE_USERS:
      return {...state, isSaving: false, isSavingCancelled: true};
    default:
      return state;
  }
};

