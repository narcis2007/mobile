package fallingstars.fallingdroid.service;

import android.content.Context;
import android.support.v4.content.Loader;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import fallingstars.fallingdroid.content.Authorization;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.net.UserRestClient;
import fallingstars.fallingdroid.util.OkCancellableCall;
import fallingstars.fallingdroid.util.OnErrorListener;
import fallingstars.fallingdroid.util.OnSuccessListener;

public class UserManager {
    private static final String TAG = UserManager.class.getSimpleName();

    private List<User> mUsers;
    private OnUserUpdateListener mOnUpdate;

    private final Context mContext;
    private UserRestClient mUserRestClient;
    String token;

    public UserManager(Context context) {
        mContext = context;
    }

    public List<User> getUsers() throws Exception { //sync method
        Log.d(TAG, "getUsers...");
        mUsers = mUserRestClient.getAll();
        return mUsers;
    }

    public UsersLoader getUsersLoader() {
        Log.d(TAG, "getUsersLoader...");
        return new UsersLoader(mContext, mUserRestClient);
    }

    public Loader<User> getUserLoader(String username) {
        Log.d(TAG, "getUserLoader...");
        return new UserLoader(mContext, mUserRestClient,username);
    }

    public void addUser() {
//        mNotes.add(new Note("Added note " + mNotes.size()));
//        if (mOnUpdate != null) {
//            mOnUpdate.updated();
//        }
    }

    public void setOnUpdate(OnUserUpdateListener onUpdate) {
        mOnUpdate = onUpdate;
    }

    public void setUserRestClient(UserRestClient userRestClient) {
        mUserRestClient = userRestClient;
    }

    public OkCancellableCall getUsersAsync(OnSuccessListener<List<User>> onSuccessListener, OnErrorListener onErrorListener) {
        return mUserRestClient.getAllAsync(onSuccessListener, onErrorListener);
    }

    public OkCancellableCall login(String username, String password, OnSuccessListener<Authorization> onSuccessListener, OnErrorListener onErrorListener) {
        return mUserRestClient.login(username, password, onSuccessListener, onErrorListener);
    }

    public User getUser(String username) throws IOException {
        return mUserRestClient.getUser(username);
    }

    public OkCancellableCall deleteUser(String username, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        return mUserRestClient.deleteUser(username, onSuccessListener, onErrorListener);
    }

    public OkCancellableCall updateUser(User user, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        return mUserRestClient.updateUser(user, onSuccessListener, onErrorListener);
    }

    public OkCancellableCall saveUser(User user, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        return mUserRestClient.saveUser(user, onSuccessListener, onErrorListener);
    }


    public interface OnUserUpdateListener {
        void updated();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.mUserRestClient.setToken(token);
    }
}
