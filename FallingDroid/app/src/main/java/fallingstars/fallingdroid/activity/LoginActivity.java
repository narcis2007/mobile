package fallingstars.fallingdroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fallingstars.fallingdroid.FallingApp;
import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.Authorization;
import fallingstars.fallingdroid.util.OnErrorListener;
import fallingstars.fallingdroid.util.OnSuccessListener;

/**
 * Created by Narcis2007 on 10.11.2016.
 */

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();
    LoginActivity self=this;

    private FallingApp fallingApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fallingApp = (FallingApp) getApplication();
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void login(View v) {
        final String username = ((EditText) findViewById(R.id.username)).getText().toString();
        String password = ((EditText) findViewById(R.id.password)).getText().toString();
        Toast.makeText(this, username + password, Toast.LENGTH_LONG).show();
        fallingApp.getUserManager().login(username, password, new OnSuccessListener<Authorization>() {
                    @Override
                    public void onSuccess(final Authorization authorization) {
                        Log.d(TAG, "login - success");
                        self.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "ok", Toast.LENGTH_LONG).show();
                                fallingApp.getUserManager().setToken(authorization.getToken());
                                Intent intent=new Intent(self,UserActivity.class);
                                startActivity(intent);
                            }
                        });

//                        ((TextView) findViewById(R.id.message)).setText("success");

                    }
                }
                , new OnErrorListener() {
                    @Override
                    public void onError(final Exception e) {
                        Log.d(TAG, "startGetUsersAsyncCall - error");
                     self.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            ((TextView) findViewById(R.id.message)).setText(e.getMessage());
                        }
                    });
                    }
                });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
