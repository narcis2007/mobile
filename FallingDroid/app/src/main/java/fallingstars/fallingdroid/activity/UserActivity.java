package fallingstars.fallingdroid.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import fallingstars.fallingdroid.FallingApp;
import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.OnActionCompletedListener;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.fragment.UserDetailFragment;
import fallingstars.fallingdroid.fragment.UserListFragment;

public class UserActivity extends AppCompatActivity {

    public static final String TAG = UserActivity.class.getSimpleName();

    private FallingApp fallingApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generateUserListFragment();


        fallingApp = (FallingApp) getApplication();
        Log.d(TAG, "onCreate");
    }

    private void generateUserListFragment() {
        UserListFragment fragment = new UserListFragment();
        fragment.setOnClickHandler((User user) -> {
            Toast.makeText(UserActivity.this, "clicked " + user.getEmail(), Toast.LENGTH_LONG).show();
            UserDetailFragment userDetailFragment = new UserDetailFragment();
            userDetailFragment.setUsername(user.getUsername());
            OnActionCompletedListener onActionCompletedListener=new OnActionCompletedListener<Object>() {
                @Override
                public void completed(Object ok) {
                    generateUserListFragment();
                }
            };
            userDetailFragment.setOnDelete(onActionCompletedListener);
            userDetailFragment.setOnSave(onActionCompletedListener);
            userDetailFragment.setOnUpdate(onActionCompletedListener);
            FragmentManager fm2 = getSupportFragmentManager();
            FragmentTransaction transaction = fm2.beginTransaction();
            transaction.replace(R.id.contentFragment, userDetailFragment);
            transaction.commit();
        });
        fragment.setOnAdd(new OnActionCompletedListener<Object>(){

            @Override
            public void completed(Object o) {
                UserDetailFragment userDetailFragment = new UserDetailFragment();
                OnActionCompletedListener onActionCompletedListener=new OnActionCompletedListener<Object>() {
                    @Override
                    public void completed(Object ok) {
                        generateUserListFragment();
                    }
                };
                userDetailFragment.setOnSave(onActionCompletedListener);
                FragmentManager fm2 = getSupportFragmentManager();
                FragmentTransaction transaction = fm2.beginTransaction();
                transaction.replace(R.id.contentFragment, userDetailFragment);
                transaction.commit();
            }
        });
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment);
        transaction.commit();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
