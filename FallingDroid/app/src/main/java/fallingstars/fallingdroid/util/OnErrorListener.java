package fallingstars.fallingdroid.util;

public interface OnErrorListener {
    void onError(Exception e);
}
