package fallingstars.fallingdroid.net;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;

import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.util.ResourceReader;

public class UserReader implements ResourceReader<User> {
    private static final String TAG = UserReader.class.getSimpleName();

    public static final String USER_ID = "id";
    public static final String USER_USERNAME = "username";
    public static final String USER_EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";

    @Override
    public User read(JsonReader reader) throws IOException {
        User user = new User();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(USER_EMAIL)) {
                user.setEmail(reader.nextString());
            } else if (name.equals(USER_ID)) {
                user.setId(reader.nextString());
            } else if (name.equals(FIRST_NAME)) {
                if(reader.peek()!=JsonToken.NULL)
                    user.setFirstName(reader.nextString());
                else
                    reader.skipValue();
            } else if (name.equals(LAST_NAME)) {
                if(reader.peek()!=JsonToken.NULL)
                    user.setLastName(reader.nextString());
                else
                    reader.skipValue();
            } else if (name.equals(USER_USERNAME)) {
                user.setUsername(reader.nextString());
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("User property '%s' ignored", name));
            }
        }
        reader.endObject();
        return user;
    }
}
