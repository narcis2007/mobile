package fallingstars.fallingdroid.content;

/**
 * Created by Narcis2007 on 19.11.2016.
 */

//public class UserClickHandler implements Runnable {
//    private final User user;
//    private final Context context;
//
//    UserClickHandler(User user, Context context){
//        this.user=user;
//        this.context=context;
//    }
//    @Override
//    public void run() {
//        Toast.makeText(context, "clicked "+user.getEmail(), Toast.LENGTH_LONG).show();
//    }
//}

@FunctionalInterface
public interface UserClickHandler{
    void handle(User user);
}