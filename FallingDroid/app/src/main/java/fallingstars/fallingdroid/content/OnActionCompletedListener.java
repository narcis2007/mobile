package fallingstars.fallingdroid.content;

/**
 * Created by Narcis2007 on 21.11.2016.
 */

public interface OnActionCompletedListener<E> {
    void completed(E e);
}
