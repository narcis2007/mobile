package fallingstars.fallingdroid.service;

import android.content.Context;
import android.util.Log;

import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.net.UserRestClient;
import fallingstars.fallingdroid.util.OkAsyncTaskLoader;

public class UserLoader extends OkAsyncTaskLoader<User> {
    private static final String TAG = UserLoader.class.getSimpleName();
    private final UserRestClient mUserRestClient;
    private final String username;
    User user;

    public UserLoader(Context context, UserRestClient userRestClient, String username) {
        super(context);
        mUserRestClient = userRestClient;
        this.username=username;
    }

    @Override
    public User tryLoadInBackground() throws Exception {
        Log.d(TAG, "tryLoadInBackground");
        user = mUserRestClient.getUser(username);
        return user;
    }

    @Override
    protected void onStartLoading() {
        if (user != null) {
            Log.d(TAG, "onStartLoading - deliver result");
            deliverResult(user);
        }

        if (takeContentChanged() || user == null) {
            Log.d(TAG, "onStartLoading - force load");
            forceLoad();
        }
    }
}
