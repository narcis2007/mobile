package fallingstars.fallingdroid.content;

/**
 * Created by Narcis2007 on 15.11.2016.
 */

public class Authorization {
    String token;

    public Authorization(){

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
