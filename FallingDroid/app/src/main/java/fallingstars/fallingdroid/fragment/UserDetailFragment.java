package fallingstars.fallingdroid.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fallingstars.fallingdroid.FallingApp;
import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.OnActionCompletedListener;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.util.OkAsyncTask;
import fallingstars.fallingdroid.util.OkAsyncTaskLoader;
import fallingstars.fallingdroid.util.OnErrorListener;
import fallingstars.fallingdroid.util.OnSuccessListener;

public class UserDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<User> {

    public static final String TAG = UserDetailFragment.class.getSimpleName();
    private static final int USER_LOADER_ID = 1;
    private FallingApp mApp;
    private AsyncTask<String, Void, User> getUserAsyncTask;
    private View mContentLoadingView;
    private boolean mContentLoaded = false;
    String username;
    User user;
    private View userView;
    private OnActionCompletedListener<Boolean> onDelete;
    private OnActionCompletedListener<User> onSave;
    private OnActionCompletedListener<User> onUpdate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserDetailFragment() {
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mApp = (FallingApp) context.getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        userView = inflater.inflate(R.layout.fragment_user, container, false);
        mContentLoadingView = userView.findViewById(R.id.content_loading);
        showLoadingIndicator();
        return userView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        userView.findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mApp.getUserManager().deleteUser(username, new OnSuccessListener<Boolean>() {
                    @Override
                    public void onSuccess(final Boolean ok) {
                        Log.d(TAG, "delete user - success");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onDelete.completed(ok);
                            }
                        });
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(Exception e) {
                        Log.d(TAG, "delete user - error");
                        showError(e);
                    }
                });
            }
        });
        userView.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user!=null&&user.getId() != null) {
                    updateUserFields(user);
                    mApp.getUserManager().updateUser(user, new OnSuccessListener<Boolean>() {
                        @Override
                        public void onSuccess(final Boolean ok) {
                            Log.d(TAG, "update user - success");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onUpdate.completed(user);
                                }
                            });
                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(Exception e) {
                            Log.d(TAG, "update user - error");
                            showError(e);
                        }
                    });
                }else{
                    user=new User();
                    updateUserFields(user);
                    mApp.getUserManager().saveUser(user, new OnSuccessListener<Boolean>() {
                        @Override
                        public void onSuccess(final Boolean ok) {
                            Log.d(TAG, "save user - success");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onSave.completed(user);
                                }
                            });
                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(Exception e) {
                            Log.d(TAG, "save user - error");
                            showError(e);
                        }
                    });
                }

            }
        });
    }

    private void updateUserFields(User user) {
        user.setLastName(String.valueOf(((TextView) userView.findViewById(R.id.lastName)).getText()));
        user.setEmail(String.valueOf(((TextView) userView.findViewById(R.id.email)).getText()));
        user.setFirstName(String.valueOf(((TextView) userView.findViewById(R.id.firstName)).getText()));
        if(username==null){
            user.setPassword(String.valueOf(((TextView) userView.findViewById(R.id.password)).getText()));
            user.setUsername(String.valueOf(((TextView) userView.findViewById(R.id.username)).getText()));
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(USER_LOADER_ID, null, this); //:) :(
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        startGetUserAsyncTask(); //:(
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        ensureGetUserAsyncTaskCancelled();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    private void startGetUserAsyncTask() {
        if (mContentLoaded) {
            Log.d(TAG, "startGetUserAsyncTask - content already loaded, return");
            return;
        }
        if (username != null)
            getUserAsyncTask = new OkAsyncTask<String, Void, User>() {

                @Override
                protected void onPreExecute() {
                    showLoadingIndicator();
                    Log.d(TAG, "GetUserAsyncTask - showLoadingIndicator");
                }

                @Override
                protected User tryInBackground(String... params) throws Exception {
                    Log.d(TAG, "GetUserAsyncTask - tryInBackground");
                    user = mApp.getUserManager().getUser(username);
                    return user;
                }

                @Override
                protected void onPostExecute(User user) {
                    Log.d(TAG, "GetUserAsyncTask - onPostExecute");
                    if (backgroundException != null) {
                        Log.e(TAG, "Get user failed");
                        showError(backgroundException);
                    } else {
                        showContent(user);
                    }
                }
            }.execute();
    }

    private void ensureGetUserAsyncTaskCancelled() {
        if (username != null)
            if (getUserAsyncTask != null && !getUserAsyncTask.isCancelled()) {
                Log.d(TAG, "ensureGetUserAsyncTaskCancelled - cancelling the task");
                getUserAsyncTask.cancel(true);
            } else {
                Log.d(TAG, "ensureGetUserAsyncTaskCancelled - task already completed or cancelled");
            }
    }

    @Override
    public Loader<User> onCreateLoader(int id, Bundle args) {
        showLoadingIndicator();
        return mApp.getUserManager().getUserLoader(username);
    }

    @Override
    public void onLoadFinished(Loader<User> loader, User users) {
        if (loader instanceof OkAsyncTaskLoader) {
            Exception loadingException = ((OkAsyncTaskLoader) loader).loadingException;
            if (loadingException != null) {
                Log.e(TAG, "Get user failed");
                showError(loadingException);
                return;
            }
            showContent(users);
        }
    }

    @Override
    public void onLoaderReset(Loader<User> loader) {
        // not used
    }


    private void showError(Exception e) {
        Log.e(TAG, "showError", e);
        new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setMessage(e.getMessage())
                .setCancelable(true)
                .create()
                .show();
    }

    private void showLoadingIndicator() {
        Log.d(TAG, "showLoadingIndicator");
        setVisibilityMainContent(View.GONE);
        mContentLoadingView.setVisibility(View.VISIBLE);
    }

    private void setVisibilityMainContent(int visibility) {
        userView.findViewById(R.id.email).setVisibility(visibility);
        userView.findViewById(R.id.firstName).setVisibility(visibility);
        userView.findViewById(R.id.lastName).setVisibility(visibility);
        userView.findViewById(R.id.saveButton).setVisibility(visibility);
        if (user != null&&user.getId()!=null)
            userView.findViewById(R.id.deleteButton).setVisibility(visibility);
        if(username==null){
            userView.findViewById(R.id.username).setVisibility(visibility);
            userView.findViewById(R.id.password).setVisibility(visibility);
        }
    }

    private void showContent(final User user) {
        Log.d(TAG, "showContent");
        ((TextView) userView.findViewById(R.id.email)).setText(user.getEmail());
        ((TextView) userView.findViewById(R.id.firstName)).setText(user.getFirstName());
        ((TextView) userView.findViewById(R.id.lastName)).setText(user.getLastName());
        setVisibilityMainContent(View.VISIBLE);
        mContentLoadingView.setVisibility(View.GONE);
//        userView.setVisibility(View.VISIBLE);
    }


    public void setOnDelete(OnActionCompletedListener<Boolean> onDelete) {
        this.onDelete = onDelete;
    }

    public void setOnSave(OnActionCompletedListener<User> onSave) {
        this.onSave = onSave;
    }

    public void setOnUpdate(OnActionCompletedListener<User> onUpdate) {
        this.onUpdate = onUpdate;
    }
}
