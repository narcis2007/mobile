import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator,TouchableHighlight} from 'react-native';
import {saveUser, cancelSaveUsers,deleteUser} from './service';
import {registerRightAction, issueText, getLogger} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('UserEdit');
const USER_EDIT_ROUTE = 'user/edit';

export class UserEdit extends Component {
    static get routeName() {
        return USER_EDIT_ROUTE;
    }

    static get route() {
        return {name: USER_EDIT_ROUTE, title: 'User Edit', rightText: 'Save'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        const nav = this.props.navigator;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {user: {...currentRoute.data}, isSaving: false};
        } else {
            this.state = {user: {username: '',email:''}, isSaving: false};
        }
        registerRightAction(this.props.navigator, this.onSave.bind(this));
    }

    render() {
        log('render');
        const state = this.state;
        let message = state.issue;
        if(this.state.user.id){
            return (
                <View style={styles.content}>
                    { state.isSaving &&
                    <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                    }
                    <Text>Email</Text>
                    <TextInput value={state.user.email} onChangeText={(text) => this.updateUserEmail(text) }></TextInput>
                    {message && <Text>{message}</Text>}
                    <TouchableHighlight style={styles.deleteButton} onPress={() => this.onDelete(state.user)}>
                        <View>
                            <Text style={styles.listItem}>Delete User {state.user.username}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            );
        }
        return (
            <View style={styles.content}>
                { state.isSaving &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                <Text>Email</Text>
                <TextInput value={state.user.email} onChangeText={(text) => this.updateUserEmail(text) }></TextInput>
                <Text>Username</Text>
                <TextInput value={state.user.username} onChangeText={(text) => this.updateUsername(text) }></TextInput>
                <Text>Password</Text>
                <TextInput value={state.user.password} onChangeText={(text) => this.updateUserPassword(text) }></TextInput>
                {message && <Text>{message}</Text>}
            </View>
        );
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const userState = store.getState().user;
            this.setState({...state, issue: userState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        //this.props.store.dispatch(cancelSaveUsers());
    }

    updateUserEmail(email) {
        let newState = {...this.state};
        newState.user.email = email;
        this.setState(newState);
    }
    updateUsername(username) {
        let newState = {...this.state};
        newState.user.username = username;
        this.setState(newState);
    }
    updateUserPassword(password) {
        let newState = {...this.state};
        newState.user.password = password;
        this.setState(newState);
    }

    onSave() {
        log('onSave');
        this.props.store.dispatch(saveUser(this.state.user)).then(() => {
            log('onUserSaved');
            if (!this.state.issue) {
                this.props.navigator.pop();
            }
        });
    }
    onDelete(user){
        log("onDelete");
        this.props.store.dispatch(deleteUser(this.state.user)).then(() => {
            log('onUserDeleted');
            if (!this.state.issue) {
                this.props.navigator.pop();
            }
        });
    }
}