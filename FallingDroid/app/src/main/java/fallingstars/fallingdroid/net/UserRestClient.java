package fallingstars.fallingdroid.net;

import android.content.Context;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import fallingstars.fallingdroid.R;
import fallingstars.fallingdroid.content.Authorization;
import fallingstars.fallingdroid.content.ErrorMessage;
import fallingstars.fallingdroid.content.User;
import fallingstars.fallingdroid.util.OkCancellableCall;
import fallingstars.fallingdroid.util.OnErrorListener;
import fallingstars.fallingdroid.util.OnSuccessListener;
import fallingstars.fallingdroid.util.ResourceListReader;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserRestClient {
    private static final String TAG = UserRestClient.class.getSimpleName();

    private final OkHttpClient mOkHttpClient;
    private final String mApiUrl;
    private final String mUserUrl;
    private final String mLoginUrl;
    String token;

    public UserRestClient(Context context) {
        mOkHttpClient = new OkHttpClient();
        mApiUrl = context.getString(R.string.api_url);
        mUserUrl = mApiUrl.concat("/users");
        mLoginUrl = mApiUrl.concat("/login");
        Log.d(TAG, "UserRestClient created");
    }

    public List<User> getAll() throws IOException { //sync operation
        Request request = new Request.Builder().url(mUserUrl).build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "getAll started");
            Response response = call.execute();
            Log.d(TAG, "getAll completed");
            JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
            return new ResourceListReader<User>(new UserReader()).read(reader);
        } catch (IOException e) {
            Log.e(TAG, "getAll failed", e);
            throw e;
        }
    }

    public OkCancellableCall login(String username, String password, final OnSuccessListener<Authorization> osl, final OnErrorListener oel) { //async operation
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        PostJson post = new PostJson();

        Call call = null;
        try {
            Request request = post.JSONRequest(mLoginUrl, user.toJSON());
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "login enqueued");

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "login failed", e);
                    oel.onError(e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "login completed");
                    if (response.isSuccessful()) {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                        osl.onSuccess(new AuthorizationReader().read(reader));
                    } else {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                        ErrorMessage errorMessage = new ErrorMessageReader().read(reader);
                        oel.onError(new Exception(errorMessage.getMessage()));
                    }


                }
            });
        } catch (Exception e) {
            Log.e(TAG, "login failed", e);
            oel.onError(e);
        } finally {
            return new OkCancellableCall(call);
        }
    }

    public OkCancellableCall getAllAsync(final OnSuccessListener<List<User>> osl, final OnErrorListener oel) { //async operation
        Request request = new Request.Builder().url(mUserUrl).addHeader("Authorization", token).build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "getAllAsync enqueued");

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "getAllAsync failed", e);
                    oel.onError(e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "getAllAsync completed");

                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                    osl.onSuccess(new ResourceListReader<User>(new UserReader()).read(reader));

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "getAllAsync failed", e);
            oel.onError(e);
        } finally {
            return new OkCancellableCall(call);
        }
    }


    public User getUser(String username) throws IOException {
        Request request = new Request.Builder().url(mUserUrl + "/" + username).addHeader("Authorization", token).build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "getUser started");
            Response response = call.execute();
            Log.d(TAG, "getUser completed");
            JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
            return new UserReader().read(reader);
        } catch (IOException e) {
            Log.e(TAG, "getUser failed", e);
            throw e;
        }
    }

    public void setToken(String token) {
        this.token = token;
    }

    public OkCancellableCall deleteUser(String username, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        Request request = new Request.Builder().url(mUserUrl + "/" + username).addHeader("Authorization", token).delete().build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "deleteUser enqueued");

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "deleteUser failed", e);
                    onErrorListener.onError(e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "deleteUser completed");

                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                    onSuccessListener.onSuccess(new ResponseReader().read(reader).getSuccess());

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "getAllAsync failed", e);
            onErrorListener.onError(e);
        } finally {
            return new OkCancellableCall(call);
        }
    }

    public OkCancellableCall updateUser(User user, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        Gson gson = new Gson();
        String userJson = gson.toJson(user);

        Request request = new Request.Builder().url(mUserUrl + "/" + user.getUsername()).addHeader("Authorization", token).put(RequestBody
                .create(MediaType.parse("application/json"),userJson)).build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "updateUser enqueued");

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "updateUser failed", e);
                    onErrorListener.onError(e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "updateUser completed");

                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                    onSuccessListener.onSuccess(new ResponseReader().read(reader).getSuccess());

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "updateUser failed", e);
            onErrorListener.onError(e);
        } finally {
            return new OkCancellableCall(call);
        }
    }

    public OkCancellableCall saveUser(User user, OnSuccessListener<Boolean> onSuccessListener, OnErrorListener onErrorListener) {
        Gson gson = new Gson();
        String userJson = gson.toJson(user);

        Request request = new Request.Builder().url(mUserUrl).addHeader("Authorization", token).post(RequestBody
                .create(MediaType.parse("application/json"),userJson)).build();
        Call call = null;
        try {
            call = mOkHttpClient.newCall(request);
            Log.d(TAG, "saveUser enqueued");

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "saveUser failed", e);
                    onErrorListener.onError(e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d(TAG, "saveUser completed");

                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                    onSuccessListener.onSuccess(new ResponseReader().read(reader).getSuccess());

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "updateUser failed", e);
            onErrorListener.onError(e);
        } finally {
            return new OkCancellableCall(call);
        }
    }
}
