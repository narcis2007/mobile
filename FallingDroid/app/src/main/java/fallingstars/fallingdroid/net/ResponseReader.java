package fallingstars.fallingdroid.net;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;

import fallingstars.fallingdroid.content.Response;
import fallingstars.fallingdroid.util.ResourceReader;

public class ResponseReader implements ResourceReader<Response> {
    private static final String TAG = ResponseReader.class.getSimpleName();

    public static final String SUCCESS = "success";

    @Override
    public Response read(JsonReader reader) throws IOException {
        Response response = new Response();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(SUCCESS)) {
                response.setSuccess(reader.nextBoolean());
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("Property '%s' ignored", name));
            }
        }
        reader.endObject();
        return response;
    }
}
